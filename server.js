var express = require('express');
var wines = require('./routes/wines');
var email = require('./emails/email');
var app = express();

app.use(express.json());
app.use(express.urlencoded());

//##########################################################
// This is for using method without using wines.js file
/*app.get('/wines', function(req, res){
   res.send([{name:'wine1'}, {name:'wine2'}]);
});
app.get('/wines/:id', function(req, res){
   res.send({id:req.params.id, name:"The Name", description:"description"});
});*/
//##########################################################

app.get('/wines', wines.findAll);
app.get('/wines/:id', wines.findById);
app.post('/wines', wines.addWine);

app.get('/email', email.sendMail);
//app.put('/wines/:id', wines.updateWine);
//app.delete('/wines/:id', wines.deleteWine);

app.listen(3000);
console.log('Listen on port 3000');

//##########################################################
// This is http server without express module
/*var http = require('http');
http.createServer(function (req, res){
	res.writeHead(200, {'Content-Type':'text/plain'});
	res.end('Hello World\n');
}).listen(3000, '127.0.0.1');
console.log('Server Running at http://127.0.0.1:3000/');*/
//##########################################################
