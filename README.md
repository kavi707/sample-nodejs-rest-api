Sample NodeJS REST API
======================

This is an sample rest api project from nodeJS.

## Installation ##

1. Clone the project and change directory to project directory
2. Install the node modules `npm install`
3. Run the server `node server.js`
